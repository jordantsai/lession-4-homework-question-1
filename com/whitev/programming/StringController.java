package com.whitev.programming;

import java.util.*;

public class StringController{
	/**
	 * 將文字倒轉輸出。
	 * 
	 * 例如輸入 "ABC" ，則會 print "CBA"
	 * 
	 */
	public void reverse(String str){
		
	}
	/**
	 * 取得一個句子每個字母占用的長度
	 * 
	 * 例如輸入 "This is a book"，則會回傳一個 list ，值分別如下 ：{4, 2, 1, 4}
	 */
	public List<Integer> getLengthList(String str){
		
	}
}